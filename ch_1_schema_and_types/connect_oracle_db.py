from sqlalchemy import create_engine
import os
import cx_Oracle

# Username 
username = 'admin'
# Password to account (hidden under system variable named below)
password = os.environ.get("ORACLE_DB_ADMIN_PWD")
# Hostname (hidden in tnsnames.ora in downloaded wallet)
hostname = 'db202107292011_high'
# Port - also from tnsnames.ora from wallet
port = "1522"
#
url = f'oracle://{username}:{password}@{hostname}:{port}'

engine = create_engine(url, echo=True)

# Test connection
with engine.connect() as con:
    print(con)


# Creating connection to local provisional sqlite db
# from sqlalchemy import create_engine
# engine = create_engine('sqlite:///:memory:')

