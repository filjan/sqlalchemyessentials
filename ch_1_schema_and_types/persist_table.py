from models import metadata
from connect_oracle_db import engine

# To create db tables you need call create_all on metadata instance
metadata.create_all(engine)

