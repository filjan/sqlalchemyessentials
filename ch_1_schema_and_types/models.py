from datetime import datetime

from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer, Numeric, String, Boolean, Identity

from sqlalchemy import DateTime
from sqlalchemy import PrimaryKeyConstraint, UniqueConstraint, CheckConstraint
from sqlalchemy import Index
from sqlalchemy import ForeignKey, ForeignKeyConstraint

metadata = MetaData()

cookies = Table(
    "cookies",
    metadata,
    # Oracle needs 'Identity' to add autoincrementation
    Column("cookie_id", Integer(), Identity(start=1), primary_key=True),
    Column("cookie_name", String(50), index=True),
    Column("cookie_recipe_url", String(255)),
    Column("cookie_sku", String(55)),
    Column("quantity", Integer()),
    Column("unit_cost", Numeric(12, 2)),
    # Alternative way to define index to optimize lookup over cookie_name
    # Index("ix_cookies_cookie_name", "cookie_name"),
    #
    # Logic constraint
    CheckConstraint("unit_cost >= 0.00", name="unit_cost_positive"),
    CheckConstraint("quantity >= 0", name="quantity_non-negative"),
)

users = Table(
    "users",
    metadata,
    Column("user_id", Integer(), Identity(start=1),primary_key=True),
    Column("username", String(15), nullable=False),
    Column("email_address", String(255), nullable=False),
    Column("phone", String(20), nullable=False),
    Column("password", String(25), nullable=False),
    Column("created_on", DateTime(), default=datetime.now),
    Column("updated_on", DateTime(), default=datetime.now, onupdate=datetime.now),
    # Alternative way to assign primary key
    PrimaryKeyConstraint("user_id", name="user_pk"),
    # Alternative way to assign uniqueness constraint
    UniqueConstraint("username", name="uix_username"),
)

# order might have user_id as foreign key, and onw order_id, and status 
orders = Table(
    "orders",
    metadata,
    Column("order_id", Integer(), Identity(start=1), primary_key=True),
    Column("user_id", ForeignKey("users.user_id")),
    Column("shipped", Boolean(), default=False),
)

# When table containes multiple ForeignKey relationships, there is strong 
# possibility that it is an association table
# Below line_items croses usage of order_id with cookie_id i.e. it may be
# relationship M:M -> one order might comprise of many cookies; one type of
# cookie may belongs to several orders
#
line_items = Table(
    "line_items",
    metadata,
    Column("line_items_id", Integer(), Identity(start=1), primary_key=True),
    Column("order_id", ForeignKey("orders.order_id")),
    Column("cookie_id", ForeignKey("cookies.cookie_id")),
    Column("quantity", Integer()),
    Column("extended_cost", Numeric(12, 2)),
    # Alternative way to impose ForeignKey
    # ForeignKeyConstraint(['order_id'], ['orders.order_id'])
)
