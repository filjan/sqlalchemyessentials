from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session
from ch_6_orm_schema_and_types.models import Cookie, Order, User, LineItem

# Prepare and execute scheme of interesting object's features
query_scheme = session.query(Order.order_id, User.username, User.phone, Cookie.cookie_name, 
                      LineItem.quantity, LineItem.extended_cost)
# Prepare joining queue scheme between tables (models)
query_join = query_scheme.join(User).join(LineItem).join(Cookie)
# Execute possible filtering
results = query_join.filter(User.username == 'cookiemon').all()
# Print results of query
for result in results:
    print(result)