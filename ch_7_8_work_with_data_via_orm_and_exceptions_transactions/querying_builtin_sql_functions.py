from sqlalchemy import func

from ch_6_orm_schema_and_types.models import Cookie
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session


# Built in functions are wrapped around columns. To get them first we need to
# acces 'func' module generator.

# SUM how many cookies are in stock
inv_count = session.query(func.sum(Cookie.quantity)).scalar()
print(f"inv_count : {inv_count}")

# COUNT how many types of cookies are available to order
types_count = session.query(func.count(Cookie.cookie_name)).scalar()
print(f"types_count : {types_count}")

# RENAME abovementioned column
types_count_rename = session.query(func.count(Cookie.cookie_name).label("Cookie types")).first()
print(f"types_count_rename.keys() : {types_count_rename.keys()}")
print(f"types_count_rename : {types_count_rename}")


