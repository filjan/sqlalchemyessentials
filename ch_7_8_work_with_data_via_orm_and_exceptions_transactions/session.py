from ch_1_schema_and_types.connect_oracle_db import engine

from sqlalchemy.orm import sessionmaker

# Sessionmaker should be used once in global scope
Session = sessionmaker(bind=engine)
# Create session - session is used to interact with the db
session = Session()

# Session doesn't connect to db until it has some clauses that require it to do


# session.close()
