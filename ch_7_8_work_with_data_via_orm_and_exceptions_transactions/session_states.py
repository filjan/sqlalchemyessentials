"""
States of data object instances:
    
transient - instance not in session not db

pending - instance has been added with add(), but hasn't been flushed or committed

persistent - instance has a correspondence in the db

detached - instance is not longer connected to the session, but has correspondence
in db

"""

from sqlalchemy import inspect

from ch_6_orm_schema_and_types.models import Cookie
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session

def check_instance_status(instance):
    insp = inspect(instance)    
    print(instance)
    for status_name in ["transient", "pending","persistent","detached"] :
        print(f"{status_name} : {getattr(insp, status_name)}")
        
if __name__ == "__main__":
    
    cc = Cookie()
    cc.cookie_name = "Cherry Pie"
    
    # Print status (should be transient)
    check_instance_status(cc)
    
    session.add(cc)
    # Print status (should be pending)
    check_instance_status(cc)
    
    session.commit()
    # Print status (should be persistent)
    check_instance_status(cc)
    
    session.expunge(cc)
    # Print status (should be expunge)
    check_instance_status(cc)