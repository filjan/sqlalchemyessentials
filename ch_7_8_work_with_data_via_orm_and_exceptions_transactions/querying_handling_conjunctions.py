from sqlalchemy import and_, or_, not_
from ch_6_orm_schema_and_types.models import Cookie
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session

# SQLAlchemy provides special logical operators and_, or_, not_
# but also logical operators:
# AND -> &
# OR -> |
# NOT -> ~
# but be carefull because Python operator precedence rules!
# and_, or_, not_ are preferable!

# AND - one way
query = session.query(Cookie).filter(Cookie.quantity > 20, Cookie.unit_cost < 0.4)

for result in query:
    print(result.cookie_name)
    
# AND - second way
query = session.query(Cookie).filter(or_(Cookie.quantity.between(10,50),Cookie.quantity.contains("chip")))

for result in query:
    print(result.cookie_name)
