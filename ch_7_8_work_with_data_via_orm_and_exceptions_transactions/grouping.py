from sqlalchemy import func

from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session
from ch_6_orm_schema_and_types.models import Order, User

# The aim is to list each user and their total number of orders
# Prepare and execute scheme by usernames and number of orders
query_scheme = session.query(User.username, func.count(Order.order_id))
# # Prepare joining
query_join = query_scheme.outerjoin(Order).group_by(User.username)
# Fetch all results
results = query_join.all()
# Print results of query
for result in results:
    print(result)