from ch_6_orm_schema_and_types.models import Cookie
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session


# Filtering is done by .FILTER(logical clause) method
record = session.query(Cookie).filter(Cookie.cookie_name=="chocolate chip")
print(f"record.first() : {record.first()}")

# CHAINING .filter() methods 
record_chain = session.query(Cookie).filter(Cookie.cookie_name != "chocolate chip").filter(Cookie.cookie_name != "peanut butter")
print(f"record_chain.all() : {record_chain.all()}")

# LIKE - part string in the string type column
record_like = session.query(Cookie).filter(Cookie.cookie_name.like("%chocolate%"))
print(f"record_like.all() : {record_like.all()}")

# FILTER_BY method works like filter, but uses attribute keyword from entity
# instead of explicitly providing the class as part of the filter expression
record = session.query(Cookie).filter_by(cookie_name = 'chocolate chip').first()
print(f"record : {record}")


#Other available ClauseElement methods are listed in page 25