from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session
from ch_6_orm_schema_and_types.models import Cookie

# Addition multiple records in bulk - much faster method than individual 'add',
# - but relationship setting and actions are ommitted
# - objects are not connected with session
# - fetching primary keys is not done by default
# - no events will be triggered
#
c1 = Cookie(cookie_name='peanut butter',
    cookie_recipe_url='http://some.aweso.me/cookie/peanut.html',
    cookie_sku='PB01',
    quantity=24,
    unit_cost=0.25)
#
c2 = Cookie(cookie_name='oatmeal raisin',
    cookie_recipe_url='http://some.okay.me/cookie/raisin.html',
    cookie_sku='EWW01',
    quantity=100,
    unit_cost=1.00)
# Bulk doesn't link added records with the session - so their id are unprintable
session.bulk_save_objects([c1,c2])
session.commit()
# Below prints "None"
print(c1.cookie_id)