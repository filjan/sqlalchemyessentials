from ch_6_orm_schema_and_types.models import Cookie

from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session


query = session.query(Cookie)
cc_cookie = query.filter(Cookie.cookie_name=="chocolate chip").first()
cc_cookie.quantity = cc_cookie.quantity + 120
session.commit()
print(f"cc_cookie.quantity after altering : {cc_cookie.quantity}")

cc_cookie.quantity = cc_cookie.quantity - 120
session.commit()
print(f"cc_cookie.quantity after reverse back : {cc_cookie.quantity}")