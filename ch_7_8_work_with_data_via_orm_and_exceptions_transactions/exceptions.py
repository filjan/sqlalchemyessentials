"""
Two main exceptions:
    - MultipleResultsFound - occurs when .one() query method returns more
        than one result
    - NoResultFound - occurs when .one() query is not able to return at least
        one result
    - DetachedInstanceError - occurs during attempt to access on an instance
        in db that is not currently attached to the database
    
"""

from sqlalchemy.exc import MultipleResultsFound, NoResultFound

from ch_6_orm_schema_and_types.models import Cookie, Order
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session

if __name__ == "__main__":

    # 1 - MultipleResultsFound
    results = session.query(Cookie).filter(Cookie.cookie_name.like("%chocolate%"))
    try:
        print(results.one())
    except MultipleResultsFound as exception:
        print("!!!")
        print(exception)
        print("!!!")
    
    # 2 - NoResultFound
    results = session.query(Cookie).filter(Cookie.cookie_name.like("%1234%"))
    try:
        print(results.one())
    except NoResultFound as exception:
        print("!!!")
        print(exception)
        print("!!!")

    # 3 - DetachedInstanceError
    result = session.query(Order).first()
    result.line_items
    session.expunge(result)
    # IT SHOULD RAISE DetachedInstanceError but didn't ?!
    result.line_items
    
