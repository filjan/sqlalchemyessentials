from sqlalchemy import text

from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session
from ch_6_orm_schema_and_types.models import User

query = session.query(User).filter(text("username='cookiemon'"))
print(query.all())

