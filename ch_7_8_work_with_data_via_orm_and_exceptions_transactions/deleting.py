from ch_6_orm_schema_and_types.models import Cookie
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session

# Define model type to find
query = session.query(Cookie)
# Filter interesting object
query = query.filter(Cookie.cookie_name == "dark chocolate chip")
# Execute query and take first occurrence
dcc_cookie = query.first()
# Execute deleting picked object
session.delete(dcc_cookie)
# Flush it to db (porovisional execution only for checking)
session.flush()
# Execute abovementioned query again
dcc_cookie = query.first()
print(f"Deleted object should be of 'None' type : {dcc_cookie}")
# Rollback deleting
session.rollback()