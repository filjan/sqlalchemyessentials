"""
When we under‐
take our first action with the session such as a query, it starts a connection and a
transaction. This means that by default, we don’t need to manually create transac‐
tions. However, if we need to handle any exceptions where part of the transaction
succeeds and another part fails or where the result of a transaction creates an excep‐
tion, then we must know how to control the transaction manually
"""

from sqlalchemy.exc import IntegrityError

from ch_6_orm_schema_and_types.models import Cookie, Order
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session


def ship_it(order_id):
    order = session.query(Order).get(order_id)
    for li in order.line_items:
        li.cookie.quantity = li.cookie.quantity - li.quantity
        session.add(li.cookie)
    order.shipped = True
    session.add(order)
    try:
        session.commit()
        print("shipped order ID: {}".format(order_id))
    # When quantity constraint is violated swithcon rollback()
    except IntegrityError as error:
        print('ERROR: {!s}'.format(error.orig))
        session.rollback()