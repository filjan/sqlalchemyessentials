from sqlalchemy import desc
from sqlalchemy.exc import MultipleResultsFound

from ch_6_orm_schema_and_types.models import Cookie
from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session


# General query on session
# Each called object is connected with session - it mean's, they might be
# altered/deleted and then persisted in db
cookies = session.query(Cookie)

# Print all cookies at once. .all() method returns list of objects
# .ALL()
print(cookies.all())

# Iterate over each cookie without .all() method
# .ALL()
for cookie in cookies:
    print(f"# {cookie} #")
    
# Return first record (if exist)
print(f".first(): {cookies.first()}")

# Return single value - in case of multiple records raise exception
# .ONE()
try:
    print(f".one(): {cookies.one()}")
except MultipleResultsFound as err:
    print(f".one(): !!! {err} !!!")
    
# Return first element if exist, in case of empty query return None, in case
# of multiple values raise exception. .SCALAR()
try:
    print(f".scalar(): {cookies.scalar()}")
except MultipleResultsFound as err:
    print(f".scalar(): !!! {err} !!!")
    
# Choosing only SEVERAL COLUMNS acc to columns in table
limited_cookies = session.query(Cookie.cookie_name, Cookie.quantity)
for name, quantity in limited_cookies:
    print(f"# {name} : {quantity} #")
    
# ORDER abovementioned 'limited_cookies' by quaintity
for name, quantity in limited_cookies.order_by(Cookie.quantity):
    print(f"# {name} : {quantity} #")
    
# ORDER abovementioned 'limited_cookies' by quaintity - in reverse
# also it's possible to using .desc() method
for name, quantity in limited_cookies.order_by(desc(Cookie.quantity)):
    print(f"# {name} : {quantity} #")
    
# LIMIT values by slicing (inefficient in case of large lists)
for name, quantity in limited_cookies.order_by(desc(Cookie.quantity))[:2]:
    print(f"# {name} : {quantity} #")

# LIMIT values by by .limit method
for name, quantity in limited_cookies.order_by(desc(Cookie.quantity)).limit(2):
    print(f"# {name} : {quantity} #")