from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session
from ch_6_orm_schema_and_types.models import Cookie

# Addition multiple records
dcc = Cookie(cookie_name='dark chocolate chip',
    cookie_recipe_url='http://some.aweso.me/cookie/recipe_dark.html',
    cookie_sku='CC02',
    quantity=1,
    unit_cost=0.75)
#
mol = Cookie(cookie_name='molasses',
    cookie_recipe_url='http://some.aweso.me/cookie/recipe_molasses.html',
    cookie_sku='MOL01',
    quantity=1,
    unit_cost=0.80)
# Add both instances
session.add(dcc)
session.add(mol)
# Flush works as commit but doesn't end transaction so records haven't been 
# inserted firmly yet
session.flush()
print(dcc.cookie_id)
print(mol.cookie_id)
