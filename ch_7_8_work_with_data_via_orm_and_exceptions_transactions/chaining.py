from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.session import session
from ch_6_orm_schema_and_types.models import Order, User, Cookie, LineItem

# The aim is to build a function which one take customer name, and returns
# it's essential contact data and information about orders.
# Function must also has option of printing details (or not) and flag about
# status of order (shipped or not)

def get_order_by_customer(customer_name: str, shipped: bool=None, details: bool=False) -> list:
    query = session.query(Order.order_id, User.username, User.phone)
    query = query.join(User)
    if details:
        # Extension ofd query thanks to ADD_COLUMNS
        query = query.add_columns(Cookie.cookie_name, LineItem.quantity, LineItem.extended_cost)
        query = query.join(LineItem).join(Cookie)
    if shipped is not None:
        query = query.where(Order.shipped == shipped)
    # query = query.join(User).join(LineItem).join(Cookie)
    results = query.filter(User.username == customer_name).all()
    return results

if __name__ == "__main__":
    print("Default action:")
    for res in get_order_by_customer('cookiemon'):
        print(res)
    print("Extended details action:")
    for res in get_order_by_customer('cookiemon', details=True):
        print(res)
    print("Extended details and only shipped action:")
    for res in get_order_by_customer('cookiemon', shipped=True, details=True):
        print(res)