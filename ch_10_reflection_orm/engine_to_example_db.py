from sqlalchemy import create_engine, inspect

# Connection string to locally saved db - here as snippet of chinook db in the 
# directory ch_5_reflection
url_db = r'sqlite:///../ch_5_reflection/Chinook_Sqlite.sqlite'
# Connection engine
engine = create_engine(url_db, echo=True)

# Print tables the chinook db comprises of
if __name__ == "__main__":
    inspector = inspect(engine)
    [print(tab_name) for tab_name in inspector.get_table_names()]