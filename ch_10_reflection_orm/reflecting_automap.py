"""
Reflection via automap cannot reflect CheckConstraint, comments or triggers,
client-side defaults or an association between a sequence and a column 
"""
from sqlalchemy.orm import Session
from sqlalchemy.ext.automap import automap_base
from engine_to_example_db import engine

Base = automap_base()
# Reflecting whole database into sqlalchemy-ORM 
Base.prepare(engine, reflect=True)

session = Session(engine)
if __name__ == "__main__":
    # Tables reflected from db
    for tab_name in Base.classes.keys():
        print(f"tab_name : {tab_name}")
    # Pring artists
    artist_tab = Base.classes.Artist
    for artist in session.query(artist_tab).limit(5):
        print(f"artist.Name : {artist.Name}  artist.ArtistId : {artist.ArtistId}")
    # Automap can reflect relationships N:1, 1:N, N:N
    one_artist = session.query(Base.classes.Artist).first()
    for album in one_artist.album_collection:
        print(f"one_artist.name : {one_artist.Name} album.Title : {album.Title}")
    
    