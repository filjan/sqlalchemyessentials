from ch_1_schema_and_types.connect_oracle_db import engine
from ch_6_orm_schema_and_types.models import Base

# To create db tables you need call create_all method on metadata from Base instance
Base.metadata.create_all(engine)