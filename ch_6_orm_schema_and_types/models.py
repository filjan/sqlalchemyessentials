from datetime import datetime

from sqlalchemy import ForeignKeyConstraint, CheckConstraint
from sqlalchemy import ForeignKey
from sqlalchemy import Column, Integer, Numeric, String, DateTime, Boolean, Identity
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

# declarative_base combines metadata container and a mapper that maps class to
# a db table

# Proper class for use with the ORM must do four things:
# 1. inherit from declarative_base
# 2. Contain __tablename__ claue - it's table name
# 3. Contain at least one Column object
# 4. Ensure at least one primary key

# The column name will be set to the name of the class attribute to which
# it's assigned


# Declarative base instance
Base = declarative_base()

# Inherit from Base
class Cookie(Base):
    # Define table name
    __tablename__ = "cookies"
    # Define attributes, and set id as primary_key
    cookie_id = Column(Integer(),  Identity(start=1), primary_key=True)
    cookie_name = Column(String(50))
    cookie_recipe_url = Column(String(255))
    cookie_sku = Column(String(55))
    quantity = Column(Integer())
    unit_cost = Column(Numeric(12, 2))
    # Set constraints
    __table_args__ = (
        CheckConstraint("unit_cost >= 0.00", name=f"{__tablename__}_unit_cost_positive"),
        CheckConstraint("quantity >= 0", name=f"{__tablename__}_quantity_positive",)
        )

    def __repr__(self):
        return "Cookie(cookie_name='{self.cookie_name}', " \
        "cookie_recipe_url='{self.cookie_recipe_url}', " \
        "cookie_sku='{self.cookie_sku}', " \
        "quantity={self.quantity}, " \
        "unit_cost={self.unit_cost})".format(self=self)


class User(Base):
    __tablename__ = "users"
    user_id = Column(Integer(),  Identity(start=1), primary_key=True)
    # Set values to be not NULL and UNIQUE
    username = Column(String(15), nullable=False, unique=True)
    email_address = Column(String(255), nullable=False)
    phone = Column(String(20), nullable=False)
    password = Column(String(25), nullable=False)
    # Set default value as value returned by function
    created_on = Column(DateTime(), default=datetime.now)
    # Set onupdate clause as calling function
    updated_on = Column(DateTime(), default=datetime.now, onupdate=datetime.now)

    def __repr__(self):
        return "User(username='{self.username}', " \
        "email_address='{self.email_address}', " \
        "phone='{self.phone}', " \
        "password='{self.password}')".format(self=self)

class Order(Base):
    __tablename__ = "orders"
    order_id = Column(Integer(), Identity(start=1), primary_key=True)
    # Defining ForeignKey just as in  ('users' correspond to the foreign table
    # name)
    #  SQLAlchemy knows to use
    # the ForeignKey we defined that matches the class we defined in the relationship. In
    # the preceding example, the ForeignKey(users.user_id), which has the users table’s
    # user_id column, maps to the User class via the __tablename__ attribute of users
    # and forms the relationship.
    user_id = Column(Integer(), ForeignKey("users.user_id"))
    shipped = Column(Boolean(), default=False)
    # This establish one-to-many relationship. Relationship bonds
    # orders.order_id with users.user_id. This relationship also establishes
    # an orders property on the User class via the backref keyword argument,
    # which is ordered by the order_id
    user = relationship("User", backref=backref("orders", order_by=order_id))
    def __repr__(self):
        return "Order(user_id={self.user_id}, " \
        "shipped={self.shipped})".format(self=self)

class LineItem(Base):
    __tablename__ = "line_items"
    line_item_id = Column(Integer(), Identity(start=1), primary_key=True)
    # One-to-many
    order_id = Column(Integer(), ForeignKey("orders.order_id"))
    # One-to-one relationship definition (look also relationship clause below)
    cookie_id = Column(Integer(), ForeignKey("cookies.cookie_id"))
    quantity = Column(Integer())
    extended_cost = Column(Numeric(12, 2))
    # One-to-many
    order = relationship("Order", backref=backref("line_items", order_by=line_item_id))
    # One-to-one relationship definition - 'uselist=False' is important
    # (probably prevents from existence same type of cookies in single order)
    cookie = relationship("Cookie", uselist=False)
    # ORM may use usual ForeignKeyConstraint, or more likely relationship clauses
    # ? test
    # __table_args__ = (ForeignKeyConstraint(['order_id'], ['orders.order_id']))    

    def __repr__(self):
        return "LineItems(order_id={self.order_id}, " \
        "cookie_id={self.cookie_id}, " \
        "quantity={self.quantity}, " \
        "extended_cost={self.extended_cost})".format(
        self=self)