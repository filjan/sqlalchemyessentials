from sqlalchemy.sql import select, func

from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine

# Select SUM of named quantity
select_sum = select([func.sum(cookies.c.quantity)])
#
with engine.connect() as con:
    result_proxy = con.execute(select_sum)
    print(f"result_proxy.keys() : {result_proxy.keys()}")
    print(f"result_proxy.scalar() : {result_proxy.scalar()}")

# Select COUNT of entity
select_count = select([func.count(cookies.c.cookie_name)])
#
with engine.connect() as con:
    result_proxy = con.execute(select_count)
    print(f"result_proxy.keys() : {result_proxy.keys()}")
    print(f"result_proxy.scalar() : {result_proxy.scalar()}")

# Select count of entity and RENAME column label
select_count_renamed = select(
    [func.count(cookies.c.cookie_name).label("cookies_count")]
)
#
with engine.connect() as con:
    result_proxy = con.execute(select_count_renamed)
    print(f"result_proxy.keys() : {result_proxy.keys()}")
    print(f"result_proxy.scalar() : {result_proxy.scalar()}")
