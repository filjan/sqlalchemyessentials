from ch_1_schema_and_types.connect_oracle_db import engine


with engine.connect() as con:
    con.execute("ALTER TABLE cookies ADD CONSTRAINT quantity_test CHECK (quantity >=0)")

with engine.connect() as con:
    con.execute("ALTER TABLE cookies DROP CONSTRAINT quantity_test")