from sqlalchemy import Integer, Numeric

from sqlalchemy import cast

from sqlalchemy.sql import select


from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine


# SQLAlchemy provides overloading for most standard Python operators

# +
select_plus = select([cookies.c.cookie_name + " TASTY!"])
with engine.connect() as con:
    result_proxy = con.execute(select_plus)
    for record in result_proxy.fetchall():
        print(f"record plus : {record}")
# ==
# !=
# <
# >
# <=
# >=
# \+
# - 
# *
# /
# %

# Computing value of various types
select_comp = select([cookies.c.cookie_name, cookies.c.unit_cost * cookies.c.quantity])
with engine.connect() as con:
    result_proxy = con.execute(select_comp)
    for record in result_proxy.fetchall():
        print(f"record comp : {record}")

# Computing value of various types with CASTing output value to requested type (and precision)
select_cast = select([cookies.c.cookie_name, cast(cookies.c.unit_cost * cookies.c.quantity, Numeric(10,1))])
with engine.connect() as con:
    result_proxy = con.execute(select_cast)
    for record in result_proxy.fetchall():
        print(f"record cast : {record}")

