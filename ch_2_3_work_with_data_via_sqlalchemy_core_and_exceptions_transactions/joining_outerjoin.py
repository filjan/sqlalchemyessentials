from sqlalchemy import select, func

from ch_1_schema_and_types.models import orders, users
from ch_1_schema_and_types.connect_oracle_db import engine


# Preparation requested columns from several tables
columns = [
    users.c.username,
    orders.c.order_id
]

# Preparation select
all_orders = select(columns)

# Chain joining several tables (outerjoin in cotradiction to join, also takes
# into account None values)
all_orders = all_orders.select_from(users.outerjoin(orders))

# Execute statement - i.e. print each user and his order
with engine.connect() as con:
    result_proxy = con.execute(all_orders)
    result = result_proxy.fetchall()
    for record in result:
        print("")
        for val, key in zip(record, result_proxy.keys()):
            print(f"{key} : {val}", end="\t")
    