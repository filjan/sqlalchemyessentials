from sqlalchemy.sql import select, update
from sqlalchemy.exc import IntegrityError
from ch_1_schema_and_types.connect_oracle_db import engine
from ch_1_schema_and_types.models import orders, users, cookies, line_items

# Let's create function realizing orders and updating database, but only
# when there is sufficient amount of cookies!

# Auxiliary query to print submitted orders
columns = [
    orders.c.order_id,
    users.c.username,
    cookies.c.cookie_name,
    line_items.c.quantity.label("Ordered quantity"),
    cookies.c.quantity.label("Available quantity"),
    orders.c.shipped,
]
select_scheme = select(columns)

# Join scheme - order of joined tables is important in searching for foreignkeys
join_scheme = orders.join(users).join(line_items).join(cookies)
with engine.connect() as con:
    result_proxy = con.execute(select_scheme.select_from(join_scheme))
    result = result_proxy.fetchall()
    for record in result:
        for val, key in zip(record, result_proxy.keys()):
            print(f"{key} : {val}", end="\t")
        print("")

# Function definition of marking shipping whole order (or not)
def ship_it(order_id):
    """
    Function checks availability of cookies number in databas, and when suffice
    number is available, changes order ship status to True
    """
    # Query scheme for cookies and their quantities available in store
    select_scheme = select(line_items.c.cookie_id, line_items.c.quantity)
    select_scheme = select_scheme.where(line_items.c.order_id == order_id)
    # Open connection to database
    with engine.connect() as con:
        # TRANSACTION BEGINNING - subsequent clauses are tracked
        transaction = con.begin()
        # Execute query for cookies names and number
        cookies_to_ship = con.execute(select_scheme).fetchall()
        # Do operations and track IntegrityError occurence
        try:
            # Iterate over cookie record to ship in throughout the order
            for cookie in cookies_to_ship:
                # Prepare update cookies table
                update_cookies_scheme = update(cookies)
                # Filter tables to only iterated cookie
                update_cookies_scheme = update_cookies_scheme.where(cookies.c.cookie_id == cookie.cookie_id)
                # Decrase number of cookies by ordered quantity (further during
                # execution this clause CheckConstraint may raise exception if
                # subtraction results negatively) 
                update_cookies_scheme = update_cookies_scheme.values(quantity=cookies.c.quantity - cookie.quantity)
                # Execute cookies update
                _ = con.execute(update_cookies_scheme)
            # Prepare update orders scheme
            update_orders_scheme = update(orders)
            # Filter table to only given order
            update_orders_scheme = update_orders_scheme.where(orders.c.order_id == order_id)
            # Change shipped status to True - because error hasn't occurred
            update_orders_scheme = update_orders_scheme.values(shipped=True)
            # Execute orders update
            _ = con.execute(update_orders_scheme)
            # Commit transaction successfully
            transaction.commit()
            # Output message - successful
            message = f"Shipped order ID: {order_id}"
        # Integrity error should be rase when ordered quantity
        # outnumbers available
        except IntegrityError as error:
            # Return each modification after transaction beginning
            transaction.rollback()
            # Output message - unsuccessful
            message = f"Integrity error occured: {error}"
        # Return message string regardless of error occurrence
        finally:
            return message
