from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine

select_scheme = cookies.select()

print(f"select_scheme : {select_scheme}")

with engine.connect() as con:
    # Execute the select scheme in general
    result_proxy = con.execute(select_scheme)
    # Retrieve records (all) from result_proxy
    results = result_proxy.fetchall()
