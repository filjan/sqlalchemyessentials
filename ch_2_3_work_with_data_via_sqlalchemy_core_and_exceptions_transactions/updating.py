from sqlalchemy import update
from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine


# Update row choosen by where clause

# Using function update
update_statement = update(cookies).where(cookies.c.cookie_name == "chocolate_chip")
update_statement = update_statement.values(quantity=(cookies.c.quantity + 120))
with engine.connect() as con:
    result_proxy = con.execute(update_statement)
    # Print number how many rows has been updated
    print(result_proxy.rowcount)

# Using method .update()
update_statement = cookies.update().where(cookies.c.cookie_name == "chocolate_chip")
update_statement = update_statement.values(quantity=(cookies.c.quantity - 120))
with engine.connect() as con:
    result_proxy = con.execute(update_statement)
    # Print number how many rows has been updated
    print(result_proxy.rowcount)

# Print record of given name
with engine.connect() as con:
    #
    result_proxy = con.execute(cookies.select().where(cookies.c.cookie_name=='chocolate_chip'))
    res = result_proxy.first()
    print(res)