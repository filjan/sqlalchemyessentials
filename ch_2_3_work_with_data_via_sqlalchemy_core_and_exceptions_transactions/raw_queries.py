from sqlalchemy import text
from ch_1_schema_and_types.connect_oracle_db import engine
from ch_1_schema_and_types.models import users

# Simple SQL query
with engine.connect() as con:
    result_proxy = con.execute("select * from orders").fetchall()
    print(result_proxy)


# Partial SQL query - select extended by raw sql clause
stmt = users.select().where(text("username='cookiemon'"))
with engine.connect() as con:
    result_proxy = con.execute(stmt).fetchall()
    print(result_proxy)


