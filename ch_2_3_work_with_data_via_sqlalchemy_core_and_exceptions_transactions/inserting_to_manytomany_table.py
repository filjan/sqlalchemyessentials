from sqlalchemy import insert

from ch_1_schema_and_types.models import orders, line_items
from ch_1_schema_and_types.connect_oracle_db import engine


# Create empty order number 1 - order belongs to user_id 1
ins = insert(orders).values(user_id=1, order_id=1)
with engine.connect() as con:
    result = con.execute(ins)
#
# Declare insert statement and list of ordered cookies - cookies belongs to
# order number 1. It's MANY TO MANY relation!
ins = insert(line_items)
order_items = [
    {"order_id": 1, "cookie_id": 16, "quantity": 2, "extended_cost": 1.00},
    {"order_id": 1, "cookie_id": 17, "quantity": 12, "extended_cost": 3.00},
]
with engine.connect() as con:
    result = con.execute(ins, order_items)
# ditto (but altered values)
ins = insert(orders).values(user_id=2, order_id=2)
with engine.connect() as con:
    result = con.execute(ins)
# ditto
ins = insert(line_items)
order_items = [
    {"order_id": 2, "cookie_id": 18, "quantity": 24, "extended_cost": 12.00},
    {"order_id": 2, "cookie_id": 25, "quantity": 6, "extended_cost": 6.00},
]
# ditto
with engine.connect() as con:
    result = con.execute(ins, order_items)

# ditto
ins = insert(orders).values(user_id=1, order_id=4)
with engine.connect() as con:
    result = con.execute(ins)
# ditto
ins = insert(line_items)
order_items = [
    {"order_id": 3, "cookie_id": 17, "quantity": 24, "extended_cost": 12.00},
    {"order_id": 3, "cookie_id": 16, "quantity": 6, "extended_cost": 6.00},
]
# ditto
with engine.connect() as con:
    result = con.execute(ins, order_items)