from sqlalchemy import delete
from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine


# Delete row chosen by where clause

# Using function delete
delete_statement = delete(cookies).where(cookies.c.cookie_name == "chocolate_chip")
with engine.connect() as con:
    result_proxy = con.execute(delete_statement)
    # Print number how many rows has been updated
    print(result_proxy.rowcount)

# Using method .delete()
delete_statement = cookies.delete().where(cookies.c.cookie_name == "chocolate_chip")
with engine.connect() as con:
    result_proxy = con.execute(delete_statement)
    # Print number how many rows has been updated
    print(result_proxy.rowcount)
