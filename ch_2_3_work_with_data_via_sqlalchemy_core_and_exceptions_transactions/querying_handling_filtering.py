from sqlalchemy.sql import select

from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine

# Filtering queries is done by adding where() statement.
# Typical where clause includes column + operator + value/column

# Given phrase as part of string in column using LIKE ClauseElement (like is case-sensitive)
select_filter_like = select([cookies]).where(cookies.c.cookie_name.like("%chocolate%"))
with engine.connect() as con:
    result_proxy = con.execute(select_filter_like)
    for record in result_proxy.fetchall():
        print(f"record like : {record}")
#
# Values BETWEEN limits clause
select_filter_bet = select([cookies]).where(cookies.c.quantity.between(20.0, 200.0))
with engine.connect() as con:
    result_proxy = con.execute(select_filter_bet)
    for record in result_proxy.fetchall():
        print(f"record bet : {record}")
#
# find only values equal to given IN  the list
select_filter_in = select([cookies]).where(cookies.c.unit_cost.in_([0.75, 1]))
with engine.connect() as con:
    result_proxy = con.execute(select_filter_in)
    for record in result_proxy.fetchall():
        print(f"record in : {record}")
#
# find only values equal to None (SQL NULL) - IS
select_filter_is = select([cookies]).where(cookies.c.unit_cost.is_(None))
with engine.connect() as con:
    result_proxy = con.execute(select_filter_is)
    for record in result_proxy.fetchall():
        print(f"record is : {record}")
#
# Substring in column - CONTAINS (like is case-sensitive) (similaro to like)
select_filter_cont = select([cookies]).where(cookies.c.cookie_name.contains("chocolate"))
with engine.connect() as con:
    result_proxy = con.execute(select_filter_cont)
    for record in result_proxy.fetchall():
        print(f"record cont : {record}")
        
# OTHERS:
    # .startswith
    # .endswith
    # .ilike # like but not case-sensitive
    # .notlike
    # .notin_()
    # .isnot()
