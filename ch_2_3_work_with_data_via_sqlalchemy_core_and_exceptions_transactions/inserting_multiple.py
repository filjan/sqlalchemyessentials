from sqlalchemy import insert

from ch_1_schema_and_types.models import cookies, users
from ch_1_schema_and_types.connect_oracle_db import engine

inventory_list = [
    {
        "cookie_name": "peanut butter",
        "cookie_recipe_url": "http://some.aweso.me/cookie/peanut.html",
        "cookie_sku": "PB01",
        "quantity": 24,
        "unit_cost": 0.25,
    },
    {
        "cookie_name": "oatmeal raisin",
        "cookie_recipe_url": "http://some.okay.me/cookie/raisin.html",
        "cookie_sku": "EWW01",
        "quantity": 100,
        "unit_cost": 1.00,
    },
]

# Example of multiple insert
ins = insert(cookies)
# Alternative way :
# ins = cookies.insert()

# Open connection and execute insert with list of dicts
with engine.connect() as con:
    result = con.execute(ins, inventory_list)

## after .execute, .inserted_primary_key_rows always returns tuple on Nones
print(f"result.inserted_primary_key_rows: {result.inserted_primary_key_rows}")

# Another example
customer_list = [
    {
        "username": "cookiemon",
        "email_address": "mon@cookie.com",
        "phone": "111-111-1111",
        "password": "password",
    },
    {
        "username": "cakeeater",
        "email_address": "cakeeater@cake.com",
        "phone": "222-222-2222",
        "password": "password",
    },
    {
        "username": "pieguy",
        "email_address": "guy@pie.com",
        "phone": "333-333-3333",
        "password": "password",
    },
]
ins = insert(users)
with engine.connect() as con:
    result = con.execute(ins, customer_list)
