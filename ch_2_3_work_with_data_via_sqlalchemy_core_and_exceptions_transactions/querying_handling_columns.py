from sqlalchemy import desc
from sqlalchemy.sql import select

from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine

# Select only SOME COLUMNS of given below names
select_some_columns = select([cookies.c.cookie_name, cookies.c.quantity])
#
with engine.connect() as con:
    result_proxy = con.execute(select_some_columns)
    print(f"result_proxy.keys() : {result_proxy.keys()}")
for record in result_proxy:
    print(f"{record.cookie_name} {record.quantity}")

# Modification of query to ORDER in (ASCENDING order)
# results acc. to given column
select_some_columns_ordered = select_some_columns.order_by(cookies.c.quantity)
#
with engine.connect() as con:
    result_proxy = con.execute(select_some_columns_ordered)
    print(f"result_proxy.keys() : {result_proxy.keys()}")
for record in result_proxy:
    print(f"{record.cookie_name} {record.quantity}")

# Modification of query to ORDER in (DESCENDING order)
# results acc. to given column
select_some_columns_ordered = select_some_columns.order_by(desc(cookies.c.quantity))
# Alternative way as methods of column:
# select_some_columns_ordered = select_some_columns.order_by(cookies.c.quantity.desc())
#
with engine.connect() as con:
    result_proxy = con.execute(select_some_columns_ordered)
    print(f"result_proxy.keys() : {result_proxy.keys()}")
for record in result_proxy:
    print(f"{record.cookie_name} {record.quantity}")

# Modification of query to LIMIT results acc. to given number of records
select_some_columns_ordered_limited = select_some_columns_ordered.limit(3)
#
with engine.connect() as con:
    result_proxy = con.execute(select_some_columns_ordered_limited)
    print(f"result_proxy.keys() : {result_proxy.keys()}")
for record in result_proxy:
    print(f"{record.cookie_name} {record.quantity}")
