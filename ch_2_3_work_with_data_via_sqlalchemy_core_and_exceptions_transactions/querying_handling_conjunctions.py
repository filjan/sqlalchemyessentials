from sqlalchemy import and_, or_, not_
from sqlalchemy.sql import select


from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine


# SQLAlchemy provides special logical operators and_, or_, not_
# but also logical operators:
# AND -> &
# OR -> |
# NOT -> ~
# but be carefull because Python operator precedence rules!
# and_, or_, not_ are preferable!

# AND - one way
select_multi = select([cookies]).where(
    and_(cookies.c.quantity > 23, cookies.c.unit_cost < 0.4)
)
with engine.connect() as con:
    result_proxy = con.execute(select_multi)
    for record in result_proxy.fetchall():
        print(f"record plus : {record}")

# AND - second way
select_multi = select([cookies]).where(
    (cookies.c.quantity > 23) & (cookies.c.unit_cost < 0.4)
)
with engine.connect() as con:
    result_proxy = con.execute(select_multi)
    for record in result_proxy.fetchall():
        print(f"record plus : {record}")
