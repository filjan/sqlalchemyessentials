from sqlalchemy import select

from ch_1_schema_and_types.models import orders, users, line_items, cookies
from ch_1_schema_and_types.connect_oracle_db import engine


# Preparation requested columns from several tables
columns = [
    orders.c.order_id,
    users.c.username,
    users.c.phone,
    cookies.c.cookie_name,
    line_items.c.quantity,
    line_items.c.extended_cost,
]

# Preparation select
cookiemon_orders = select(columns)

# Chain joining several tables
cookiemon_orders = cookiemon_orders.select_from(
    orders.join(users).join(line_items).join(cookies)
)

# Imposing proper filtering
cookiemon_orders = cookiemon_orders.where(users.c.username == "cookiemon")

# Execute statemnt and print each joined record
with engine.connect() as con:
    result_proxy = con.execute(cookiemon_orders)
    result = result_proxy.fetchall()
    for record in result:
        print("")
        for val, key in zip(record, result_proxy.keys()):
            print(f"{key} : {val}", end="\t")
    