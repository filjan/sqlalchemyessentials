"""
A ResultProxy is a wrapper around a DBAPI cursor object, and
its main goal is to make it easier to use and manipulate
the results of a statement.
"""

from sqlalchemy.sql import select

from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine

select_scheme = select([cookies])

print(f"select_scheme : {select_scheme}")

with engine.connect() as con:
    # Execute the select scheme in general
    result_proxy = con.execute(select_scheme)
    # Retrieve records (all) from result_proxy
    results = result_proxy.fetchall()

