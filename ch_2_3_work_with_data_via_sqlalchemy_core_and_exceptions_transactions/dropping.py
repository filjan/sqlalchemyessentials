from sqlalchemy import text
from ch_1_schema_and_types.connect_oracle_db import engine

# First version of tables had been created erroneosly - so they nedded to be
# removed and created again. For this reason belowmentioned statement were
# indispensable:

with engine.connect() as con:
    con.execute(text("DROP TABLE line_items CASCADE CONSTRAINTS"))

