from sqlalchemy import select, func

from ch_1_schema_and_types.models import orders, users, cookies, line_items
from ch_1_schema_and_types.connect_oracle_db import engine


# Query chaining is particularly useful when you are applying logic when building
# up a query


def get_orders_by_customer(cust_name, shipped=None, details=False):
    """
    Assume a function that got a list of orders for us  
    cust_name - customer to be considered
    shipped - extract order status
    details - extract details of order
    """
    # Columns to be extracted from databse
    columns = [orders.c.order_id, users.c.username, users.c.phone]
    # Join orders table to users
    joins = users.join(orders)
    # Optionally extract order details
    if details:
        # Extend list of interesting columns
        columns.extend(
            [cookies.c.cookie_name, line_items.c.quantity, line_items.c.extended_cost]
        )
        # Extend joins for extra tables
        joins = joins.join(line_items).join(cookies)
    # Prepare select statement of values
    cust_orders = select(columns)
    # Extend select statementy by necessery sources
    cust_orders = cust_orders.select_from(joins)
    # Filter query to only one given user (given by cust_name)
    cust_orders = cust_orders.where(users.c.username == cust_name)
    # Get only shipped orders
    if shipped is not None:
        cust_orders = cust_orders.where(orders.c.shipped == shipped)
    # Execute query and fetch result
    with engine.connect() as con:
        result = con.execute(cust_orders).fetchall()
    #
    return result

print(get_orders_by_customer('cookiemon'))
print(get_orders_by_customer('cakeeater', shipped=False))
print(get_orders_by_customer('cakeeater', shipped=True))
print(get_orders_by_customer('pieguy', shipped=True, details = True))
print(get_orders_by_customer('cookiemon', shipped=False, details = True))