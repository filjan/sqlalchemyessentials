"""
Order of creation:

inserting_function.py
inserting_method.py
inserting_execute.py
inserting_multiple.py

querying_select_function.py
querying_select_method.py

querying_handling_rows.py
querying_handling_columns.py
querying_handling_builtin_sql_functions.py
querying_handling_filtering.py
querying_handling_operators.py
querying_handling_conjunctions.py

updating.py
deleting.py
dropping.py <- connect with raw text or extend of DropTable of drop_all
inserting_to_manytomany_table.py
joining_join.py
joining_outerjoin.py
aliases.py
grouping.py
chaining.py
raw_queries.py

alter_table_without_migration.py
transactions.py
"""