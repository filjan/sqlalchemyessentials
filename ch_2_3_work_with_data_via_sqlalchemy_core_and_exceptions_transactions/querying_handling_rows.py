from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine

from querying_select_function import results, result_proxy

# Print number of records from query (fetched only)
print(f"result_proxy.rowcount : {result_proxy.rowcount}")

# Access to first row
first_row = results[0]
# Access to column by index
first_col_ix = first_row[1]
# Access to column by name
first_col_nm_1 = first_row.cookie_name
first_col_nm_2 = first_row["cookie_name"]
# Access to column by Column object
first_col_obj = first_row[cookies.c.cookie_name]


### Handling with non fetchedALL result proxy

# Proxy include inter alia keys (names of quered columns)
with engine.connect() as con:
    result_proxy = con.execute(cookies.select())
    print(f"result_proxy.keys() : {result_proxy.keys()}")

# Result proxy is iterable (and more efficient over fetchall)
with engine.connect() as con:
    result_proxy = con.execute(cookies.select())
    for record in result_proxy:
        print(f"record.cookie_name : {record.cookie_name}")

# Fetch first row and lease cursor OPEN
with engine.connect() as con:
    result_proxy = con.execute(cookies.select())
    result_1 = result_proxy.fetchone()
    print(f"result_1 fetchone : {result_1}")
    print(f"result_proxy.rowcount : {result_proxy.rowcount}")
    result_2 = result_proxy.fetchone()
    print(f"result_2 fetchone : {result_2}")
    print(f"result_proxy.rowcount : {result_proxy.rowcount}")

# scalar method returs SINGLE value
with engine.connect() as con:
    result_proxy = con.execute(cookies.select())
    result = result_proxy.scalar()
    print(f"result scalar : {result}")
    print(f"result_proxy.rowcount : {result_proxy.rowcount}")

# scalar method returns FIRST value
with engine.connect() as con:
    result_proxy = con.execute(cookies.select())
    result = result_proxy.first()
    print(f"result first : {result}")
    print(f"result_proxy.rowcount : {result_proxy.rowcount}")
