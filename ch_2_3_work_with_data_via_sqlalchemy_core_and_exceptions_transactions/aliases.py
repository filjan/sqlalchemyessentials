from ch_1_schema_and_types.models import users
from ch_1_schema_and_types.connect_oracle_db import engine


# User defined alias
user_tab = users.alias("u")
stmt = user_tab.select().where(user_tab.c.username == "cookiemon")

# Throughout the print result, 'u' alias replaces 'users'
print(stmt)

# Automatic alias
user_tab = users.alias()
stmt = user_tab.select().where(user_tab.c.username == "cookiemon")
print(stmt)

# Execute statement 
with engine.connect() as con:
    result_proxy = con.execute(stmt)
    result = result_proxy.fetchall()
    for record in result:
        print("")
        for val, key in zip(record, result_proxy.keys()):
            print(f"{key} : {val}", end="\t")