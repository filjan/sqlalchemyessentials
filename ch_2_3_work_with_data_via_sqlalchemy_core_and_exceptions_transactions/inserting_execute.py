from sqlalchemy import insert

from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine


# Example of insert using only declared function/method
ins = insert(cookies)
# Alternative way :
# ins = cookies.insert()

# Open connection and execute insert with given values
with engine.connect() as con:
    result = con.execute(
        ins,
        # cookie_id = 2,
        cookie_name="dark chocolate chip",
        cookie_recipe_url="http://some.aweso.me/cookie/recipe_dark.html",
        cookie_sku="CC02",
        quantity=1,
        unit_cost=0.75,
    )

print(f"result.inserted_primary_key : {result.inserted_primary_key}")
