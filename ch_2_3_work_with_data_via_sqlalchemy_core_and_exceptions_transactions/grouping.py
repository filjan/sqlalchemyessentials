from sqlalchemy import select, func

from ch_1_schema_and_types.models import orders, users
from ch_1_schema_and_types.connect_oracle_db import engine


# Preparation requested columns from several tables - here also new count column
# Aggregation via count (in the same way we can use sum)
columns = [
    users.c.username,
    func.count(orders.c.order_id)
]

# Preparation select
all_orders = select(columns)

# Chain joining several tables (outerjoin in cotradiction to join, also takes
# into account None values)
all_orders = all_orders.select_from(users.outerjoin(orders))

# Group count by users
all_orders = all_orders.group_by(users.c.username)

# Execute statement 
with engine.connect() as con:
    result_proxy = con.execute(all_orders)
    result = result_proxy.fetchall()
    for record in result:
        print("")
        for val, key in zip(record, result_proxy.keys()):
            print(f"{key} : {val}", end="\t")
    