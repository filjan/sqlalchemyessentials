from sqlalchemy import insert

from ch_1_schema_and_types.models import cookies
from ch_1_schema_and_types.connect_oracle_db import engine


# Example of insert FUNCTION
ins = insert(cookies).values(
    # cookie_id = 2,
    cookie_name = 'chocolate_chip',
    cookie_recipe_url="http://some.aweso.me/cookie/recipe.html",
    cookie_sku="CC01",
    quantity=12,
    unit_cost=0.50
    )

# Open connection and execute insert
with engine.connect() as con:
    result = con.execute(ins)
    print(result)
