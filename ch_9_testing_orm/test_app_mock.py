import unittest
from unittest import mock

from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.chaining import (
    get_order_by_customer,
)


class TestApp(unittest.TestCase):
    cookie_orders = [(1, u"cookiemon", u"111-111-1111")]
    cookie_details = [
        (1, u"cookiemon", u"111-111-1111", u"dark chocolate chip", 2, 1.0),
        (1, u"cookiemon", u"111-111-1111", u"oatmeal raisin", 12, 3.0),
    ]

    @mock.patch(
        "ch_7_8_work_with_data_via_orm_and_exceptions_transactions.chaining.session"
    )
    def test_orders_by_customer(self, mock_dal):
        mock_dal.query.return_value.join.return_value.filter.return_value.all.return_value = (
            self.cookie_orders
        )
        results = get_order_by_customer("cookiemon")
        self.assertEqual(results, self.cookie_orders)
