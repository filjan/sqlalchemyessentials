"""
to activate unittest type:
    ! python -m unittest ch_9_testing_orm/test_app.py
    being in sqlalchemyessentials as workingdir
"""

import unittest

from ch_7_8_work_with_data_via_orm_and_exceptions_transactions.chaining import (
    get_order_by_customer,
)
from ch_9_testing_orm.db import dal, prep_db


class TestApp(unittest.TestCase):

    # Correct answers used in asserts
    cookie_orders = [(1, u"cookiemon", u"111-111-1111")]
    cookie_details = [
        (1, u"cookiemon", u"111-111-1111", u"dark chocolate chip", 2, 1.00),
        (1, u"cookiemon", u"111-111-1111", u"oatmeal raisin", 12, 3.0),
    ]

    @classmethod
    def setUpClass(cls):
        """
        Connect to db in testing purpose
        """
        # Initialize connection to database for testing purposes
        dal.connect(for_test=True)
        # Initiate session
        dal.session = dal.Session()
        prep_db(dal.session)
        dal.session.close()

    # Special tearDownClass method runs once after all tests cases
    @classmethod
    def tearDownClass(cls):
        # Initialize connection to databe for testing purposes
        dal.db_drop_tables(for_test=True)

    # Initialize new session before each test case
    def setUp(self):
        dal.session = dal.Session()

    # Close session after each test case
    def tearDown(self):
        dal.session.rollback()
        dal.session.close()

    # Unittest expects each test case to begin with 'test_'
    def test_orders_by_customer_blank(self):
        results = get_order_by_customer("")
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_shipped(self):
        results = get_order_by_customer("", True)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_notshipped(self):
        results = get_order_by_customer("", False)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_details(self):
        results = get_order_by_customer("", details=True)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_shipped_details(self):
        results = get_order_by_customer("", True, True)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_notshipped_details(self):
        results = get_order_by_customer("", False, True)
        self.assertEqual(results, [])

    def test_orders_by_customer(self):
        results = get_order_by_customer("cookiemon")
        self.assertEqual(results, self.cookie_orders)

    def test_orders_by_customer_shipped_only(self):
        results = get_order_by_customer("cookiemon", True)
        self.assertEqual(results, [])

    def test_orders_by_customer_unshipped_only(self):
        results = get_order_by_customer("cookiemon", False)
        self.assertEqual(results, self.cookie_orders)

    def test_orders_by_customer_with_details(self):
        results = get_order_by_customer("cookiemon", details=True)
        self.assertEqual(results, self.cookie_details)

    def test_orders_by_customer_shipped_only_with_details(self):
        results = get_order_by_customer("cookiemon", True, True)
        self.assertEqual(results, [])

    def test_orders_by_customer_unshipped_only_details(self):
        results = get_order_by_customer("cookiemon", False, True)
        self.assertEqual(results, self.cookie_details)
