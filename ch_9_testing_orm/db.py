from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from ch_1_schema_and_types.connect_oracle_db import url
from ch_6_orm_schema_and_types.models import Base
from ch_6_orm_schema_and_types.models import Order, User, Cookie, LineItem

class DataAccessLayer:

    # Special code added at end of each table name - only in testing cases
    test_code = "_119ytr2bcg4lfr0afuns"
    
    def __init__(self):
        self.engine = None
        self.conn_string = url
        
    def connect(self, for_test=False):
        self.engine = create_engine(self.conn_string, echo=True)

        if for_test:
            for key, tab in Base.metadata.tables.items():
                tab.name += self.test_code

        Base.metadata.create_all(self.engine)
        self.Session=sessionmaker(bind=self.engine)


    def db_drop_tables(self, for_test=False):
        """
        Method is necessary to drop tables created for testing purposes.
        Method seeks for tables with names ended with special code at the end.
        Each found table is dropped with cointains.
        """
        self.engine = create_engine(self.conn_string, echo=True)
        tabs_to_drop = []
        if for_test:
            for tab in Base.metadata.tables.values():
                if tab.name.endswith(dal.test_code):
                    tabs_to_drop.append(tab)
        Base.metadata.drop_all(bind=self.engine, tables=tabs_to_drop)
                

dal= DataAccessLayer()

def prep_db(session):
    """
    Insert some values for testing purposes
    """
    c1 = Cookie(cookie_name='dark chocolate chip',
        cookie_recipe_url='http://some.aweso.me/cookie/dark_cc.html',
        cookie_sku='CC02',
        quantity=1,
        unit_cost=0.75)
    c2 = Cookie(cookie_name='peanut butter',
        cookie_recipe_url='http://some.aweso.me/cookie/peanut.html',
        cookie_sku='PB01',
        quantity=24,
        unit_cost=0.25)
    c3 = Cookie(cookie_name='oatmeal raisin',
        cookie_recipe_url='http://some.okay.me/cookie/raisin.html',
        cookie_sku='EWW01',
        quantity=100,
        unit_cost=1.00)
    session.bulk_save_objects([c1, c2, c3])
    session.commit()
    
    cookiemon = User(username='cookiemon',
        email_address='mon@cookie.com',
        phone='111-111-1111',
        password='password')
    cakeeater = User(username='cakeeater',
        email_address='cakeeater@cake.com',
        phone='222-222-2222',
        password='password')
    pieperson = User(username='pieperson',
        email_address='person@pie.com',
        phone='333-333-3333',
        password='password')
    session.add(cookiemon)
    session.add(cakeeater)
    session.add(pieperson)
    session.commit()
    
    o1 = Order()
    o1.user = cookiemon
    session.add(o1)
    line1 = LineItem(cookie=c1, quantity=2, extended_cost=1.00)
    line2 = LineItem(cookie=c3, quantity=12, extended_cost=3.00)
    o1.line_items.append(line1)
    o1.line_items.append(line2)
    session.commit()
    o2 = Order()
    o2.user = cakeeater
    line1 = LineItem(cookie=c1, quantity=24, extended_cost=12.00)
    line2 = LineItem(cookie=c3, quantity=6, extended_cost=6.00)
    o2.line_items.append(line1)
    o2.line_items.append(line2)
    session.add(o2)
    session.commit()