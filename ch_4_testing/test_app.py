"""
Code has been slightly modified from original from book:
    - tearDownClass metod has been added to drop tables created only for 
    test purpose.
"""
import unittest

from decimal import Decimal

from ch_1_schema_and_types.connect_oracle_db import url
from ch_3_testing.db import dal, prep_db
from ch_3_testing.app import get_orders_by_customer

# Test clasess must inherit from unittest.TestCase
class TestApp(unittest.TestCase):
    """
    To run test type in spyder console :
        ! python -m unittest ch_3_testing/test_app.py
    but be careful! Your working directory must by one folder up !!!
    """

    # Special setUpClass runs once before any test case
    @classmethod
    def setUpClass(cls):
        # Initialize connection to database for testing purposes
        dal.db_init(url, for_test=True)
        # Insert some example values into db
        prep_db()

    # Special tearDownClass method runs once after all tests cases
    @classmethod
    def tearDownClass(cls):
        # Initialize connection to databe for testing purposes
        dal.db_drop_tables(for_test=True)

    # Unittest expects each test case to begin with 'test_'
    def test_orders_by_customer_blank(self):
        results = get_orders_by_customer("")
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_shipped(self):
        results = get_orders_by_customer("", True)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_notshipped(self):
        results = get_orders_by_customer("", False)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_details(self):
        results = get_orders_by_customer("", details=True)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_shipped_details(self):
        results = get_orders_by_customer("", True, True)
        self.assertEqual(results, [])

    def test_orders_by_customer_blank_notshipped_details(self):
        results = get_orders_by_customer("", False, True)
        self.assertEqual(results, [])

    def test_orders_by_customer(self):
        expected_results = [(1, u"cookiemon", u"111-111-1111")]
        results = get_orders_by_customer("cookiemon")
        self.assertEqual(results, expected_results)

    def test_orders_by_customer_shipped_only(self):
        results = get_orders_by_customer("cookiemon", True)
        self.assertEqual(results, [])

    def test_orders_by_customer_unshipped_only(self):
        expected_results = [(1, u"cookiemon", u"111-111-1111")]
        results = get_orders_by_customer("cookiemon", False)
        self.assertEqual(results, expected_results)

    def test_orders_by_customer_with_details(self):
        expected_results = [
            (
                1,
                u"cookiemon",
                u"111-111-1111",
                u"dark chocolate chip",
                2,
                Decimal("1.00"),
            ),
            (
                1,
                u"cookiemon",
                u"111-111-1111",
                u"oatmeal raisin",
                12,
                Decimal("3.00"),
            ),
        ]
        results = get_orders_by_customer("cookiemon", details=True)
        self.assertEqual(results, expected_results)

    def test_orders_by_customer_shipped_only_with_details(self):
        results = get_orders_by_customer("cookiemon", True, True)
        self.assertEqual(results, [])

    def test_orders_by_customer_unshipped_only_details(self):
        expected_results = [
            (
                1,
                u"cookiemon",
                u"111-111-1111",
                u"dark chocolate chip",
                2,
                Decimal("1.00"),
            ),
            (
                1,
                u"cookiemon",
                u"111-111-1111",
                u"oatmeal raisin",
                12,
                Decimal("3.00"),
            ),
        ]
        results = get_orders_by_customer("cookiemon", False, True)
        self.assertEqual(results, expected_results)
