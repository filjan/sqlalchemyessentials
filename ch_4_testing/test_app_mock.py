import unittest
from decimal import Decimal

from unittest import mock

from ch_3_testing.db import dal, prep_db
from ch_3_testing.app import get_orders_by_customer

class TestApp(unittest.TestCase):
    cookie_orders = [(u'wlk001', u'cookiemon', u'111-111-1111')]
    cookie_details = [
    (u'wlk001', u'cookiemon', u'111-111-1111',
    u'dark chocolate chip', 2, Decimal('1.00')),
    (u'wlk001', u'cookiemon', u'111-111-1111',
    u'oatmeal raisin', 12, Decimal('3.00'))
    ]

    # Patching dal.connection in the app module with a mock.
    # Mock is passed into test test case as mock_conn
    @mock.patch('ch_3_testing.app.dal.connection')
    def test_orders_by_customer(self, mock_conn):
        # Return value of execute method is chained to returned value of the fetchall method,
        # then is set to self.cookie_orders
        mock_conn.execute.return_value.fetchall.return_value = self.cookie_orders
        # Function is called, where dal.connection is mocked and return abovemention
        # values
        results = get_orders_by_customer('cookiemon')
        self.assertEqual(results, self.cookie_orders)

    # Mocking select function and connection method
    @mock.patch('ch_3_testing.app.select')
    @mock.patch('ch_3_testing.app.dal.connection')
    # Decorators are ordered as stack from down, so first argument corresponf
    # to connection, and second to select
    def test_orders_by_customer_blank(self, mock_conn, mock_select):
        # Mock: cust_orders = cust_orders.select_from(joins).where(dal.users.c.username == cust_name
        mock_select.return_value.select_from.return_value.where.return_value = ''
        # Mock: result = dal.connection.execute(cust_orders).fetchall()
        mock_conn.execute.return_value.fetchall.return_value = []
        results = get_orders_by_customer('')
        self.assertEqual(results, [])        