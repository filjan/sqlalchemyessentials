from sqlalchemy import MetaData

from ch_5_reflection.engine_to_example_db import engine

metadata = MetaData()

# In order to reflect whole database, you can use reflect method on metadata
# instance
metadata.reflect(bind=engine)

[print(key) for key in metadata.tables.keys()]


# To query individual table we must assign them into variables e.g:
track = metadata.tables["Track"]
select_schema = track.select().limit(10)
result = engine.execute(select_schema).fetchall()
[print(record) for record in result]
