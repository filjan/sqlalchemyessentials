from sqlalchemy import create_engine

# Connection string to locally saved db - here as snippet of chinook db in the 
# same directory
url_db = 'sqlite:///Chinook_Sqlite.sqlite'
# Connection engine
engine = create_engine(url_db)

# Print tables the chinook db comprises of
if __name__ == "__main__":
    [print(tab_name) for tab_name in engine.table_names()]