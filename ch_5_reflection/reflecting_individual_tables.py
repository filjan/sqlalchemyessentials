from sqlalchemy import Table, MetaData

from ch_5_reflection.engine_to_example_db import engine

# Metadata and engine instance are indispensable stuff to do reflecting

# Create empty metadata container for reflected tabl
metadata = MetaData()

# Autoload & autoload_with reflect the schema from db(engine) into metadata, of
# one table - here with name "Artist". In case of lack of the table of given
# name, NoSuchTableError is risen
# autoload - copy records and then add (or replace) them into metadata
artist = Table("Artist", metadata, autoload=True, autoload_with=engine)

select_scheme = artist.select().limit(20)
results = engine.execute(select_scheme).fetchall()
[print(record) for record in results]
