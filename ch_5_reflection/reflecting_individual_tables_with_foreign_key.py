from sqlalchemy import Table

from ch_5_reflection.engine_to_example_db import engine
from ch_5_reflection.reflecting_individual_tables import artist, metadata


# Example of table with foreign key
album = Table("Album", metadata, autoload=True, autoload_with=engine)
# Print details of reflected Album table
print(f"{metadata.tables['Album'].__repr__}")
print(f"Show foreign keys: {album.foreign_keys}")
# In previous versions foreign keys hadn't been reflected at once. To do it
# manually it's done by ForeignKeyConstraint like:
# album.append_constraint(ForeignKeyConstraint(['ArtistId'], ['artist.ArtistId']))

select_scheme = artist.join(album).select().limit(20)
results = engine.execute(select_scheme).fetchall()
[print(record) for record in results]